/** 入力欄のPlaceholder */
export const INPUT_PLACEHOLDER = `.
  # Assets files
  assets
    images
    css
    fonts
  # Vue components
  components
    Atoms
    Molecules
    Organisms
  # Page components
  containers
    ...
  # Utilities
  utils
    # Entry file
    index.ts
    ...
  # Node.js packages
  node_modules
  # Node.js configuration file
  package.json
  # Documentation (this file)
  README.md`;

/** プレビュー欄のPlaceholder */
export const PREVIEW_PLACEHOLDER = `.
│   # Assets files
├── assets
│   ├── images
│   ├── css
│   └── fonts
│   # Vue components
├── components
│   ├── Atoms
│   ├── Molecules
│   └── Organisms
│   # Page components
├── containers
│   └── ...
│   # Utilities
├── utils
│   │   # Entry file
│   ├── index.ts
│   └── ...
│   # Node.js packages
├── node_modules
│   # Node.js configuration file
├── package.json
│   # Documentation (this file)
└── README.md`;
